const Discord = require('discord.js');
const client = new Discord.Client();

const commands = require('./commands');

client.on('channelCreate', () => {
});
client.on('channelDelete', () => {
});
client.on('channelPinsUpdate', () => {
});
client.on('channelUpdate', () => {
});
client.on('clientUserGuildSettingsUpdate', () => {
});
client.on('clientUserSettingsUpdate', () => {
});
client.on('debug', () => {
});
client.on('debug', () => {
});
client.on('disconnect', () => {
});
client.on('emojiCreate', () => {
});
client.on('emojiDelete', () => {
});
client.on('emojiUpdate', () => {
});
client.on('error', () => {
});
client.on('guildBanAdd', () => {
});
client.on('guildBanRemove', () => {
});
client.on('guildCreate', () => {
});
client.on('guildDelete', () => {
});
client.on('guildIntegrationsUpdate', () => {
});
client.on('guildMemberAdd', (member) => {
    const channel = member.guild.channels.find(ch => ch.name === 'member-log');
    if (!channel) return;
    channel.send(`Welcome to the server, ${member}!`)

    member.send(`In accordance with GDPR I'd like to inform you that I'll be keeping my eye out for you.
    
    My eyes will see things like who you mention in chat messages in order to add functionality to the bot.
    
    Don't worry, I don't write down message contents, I mostly just add it in the form of "person A has mentioned person B x number of times" in order to e.g. calculate how lewd the two of you are with one another.
    
    If you agree to my eye being on you, then add the "Accept" reaction below.
    
    You can at any time disagree to the privacy statement to stop complying, or even request a deletion of all my data on you, or an excerpt of all my data on you. Write "+lewd privacy" for more information.`);
});
client.on('guildMemberAvailable', () => {
});
client.on('guildMemberRemove', () => {
});
client.on('guildMembersChunk', () => {
});
client.on('guildMemberSpeaking', () => {
});
client.on('guildMemberUpdate', () => {
});
client.on('guildUnavailable', () => {
});
client.on('guildUpdate', () => {
});
client.on('message', message => {
    if (message.content.substr(0, 5) === '!lewd') {
        message.content = message.content.substr(6);
        commands.execute(message);
    }
});
client.on('messageDelete', () => {
});
client.on('messageDeleteBulk', () => {
});
client.on('messageReactionAdd', () => {
});
client.on('messageReactionRemove', () => {
});
client.on('messageReactionRemoveAll', () => {
});
client.on('messageUpdate', () => {
});
client.on('presenceUpdate', () => {
});
client.on('rateLimit', () => {
});
client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);
});
client.on('reconnecting', () => {
});
client.on('resume', () => {
});
client.on('roleCreate', () => {
});
client.on('roleDelete', () => {
});
client.on('roleUpdate', () => {
});
client.on('typingStart', (channel, user) => {

});
client.on('typingStop', (channel, user) => {

});
client.on('userNoteUpdate', () => {
});
client.on('userUpdate', () => {
});
client.on('userUpdate', () => {
});
client.on('voiceStateUpdate', () => {
});
client.on('warn', () => {
});
client.on('webhookUpdate', () => {
});

// Logs the bot onto Discord
client.login(process.env.LEWDBOT_TOKEN);
