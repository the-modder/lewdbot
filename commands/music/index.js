module.exports = client => {
    client.on('message', message => {
        if (message.content === '!lewd music') {
            const channel = message.guild.channels.find(ch => ch.name === 'bot-music');
            channel.join()
                .then(connection => {
                    message.reply('I have successfully connected to the channel!');
                })
                .catch(console.log);
        }
    });
};

module.exports.help = `\`!lewd music [url]\`

Plays back the audio of a specified URL by using \`youtube-dl\` to first download the video then plays it on the users current voice channel.

**Notes!**
  - Video files larger than 1 GB are not allowed.
  - Video files are deleted if they are either older than 3 hours old or if the total size of all video files exceeds 10 GB
  - \`youtube-dl\` supports more sites than just YouTube!!!`;
