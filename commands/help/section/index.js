module.exports = message => {
    if(!message.content) {
        message.author.send('You must specify a section!');
    } else {
        message.author.send(require('./' + message.content));
    }
};

module.exports.help = `\`!lewd help section [section]\`

Shows you a list of commands for `;
