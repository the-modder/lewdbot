module.exports = message => {
    if(!message.content) {
        message.content = 'help';
    }
    message.author.send(require('../' + message.content.split(' ').join('/')).help);
};

module.exports.help = `For more info on a specific command, use \`!lewd help {command}\`
For more info on all commands of a section, use \`!lewd help section {section_title}\`

(Some of the below commands are merely planned and not yet implemented at all)

**Command List**
:video_game: Games
\`games\`
:writing_hand: Roleplaying
\`d20\` \`roll\` \`characters\` \`character\` \`character creation\`
:musical_note: Music
\`music\` \`dj\`
:joy: Meme Generation
\`random\`
:sleeping_accommodation: Bedtime
\`story\`
:wrench: Utility
\`ping\` \`link\``;
