module.exports = (message) => {

};

module.exports.help = `\`!lewd story [category] [interactive] [language]\`

The bot will read a story for you.

With the \`interactive\` modifier the story becomes interactive. You make choices in the story using reactions.

**Categories**
\`fairy\` - Fairytale
\`fable\` - Fables
\`horror\` - Horror stories
\`creepy\` - Creepy Pasta's

**Languages**
In case a story has not been translated yet it will be auto translated by Yandex.
\`en\` - English
\`sv\` - Swedish
\`dk\` - Danish`;